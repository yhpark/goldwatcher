const { createConnection } = require('typeorm')
const Asset = require('entity/Asset')

describe('Asset', () => {
  beforeAll(() => createConnection())

  let testCnt = 0

  function insertAsset (key = 'TestBank' + (testCnt++), unit = 'KRW') {
    return Asset.create({ key, unit }).save()
  }

  test('Insert', async () => {
    const asset = await insertAsset()

    expect(asset.hasId()).toBeDefined()
  })

  test('Insert duplicate key', async () => {
    await insertAsset('Duplicate Bank')

    await expect(insertAsset('Duplicate Bank')).rejects.toThrow('UNIQUE')
  })

  test('update version', async () => {
    const assetOld = await insertAsset()

    const assetNew = assetOld.updateVersion()
    assetNew.unit = 'USD'
    await assetNew.save()

    expect(assetNew.id).toBeDefined()
    expect(assetNew.id).not.toBe(assetOld.id)
    expect(assetNew.key).toBe(assetOld.key)
    expect(assetNew.created).toBe(assetOld.created)
    expect(assetNew.updated.getTime()).toBeGreaterThan(assetOld.updated.getTime())
    expect(assetNew.version).toBe(assetOld.version + 1)
  })

  test('update version, find the latest one by key', async () => {
    const assetOld = await insertAsset()

    const assetNew = assetOld.updateVersion()
    assetNew.unit = 'USD'
    await assetNew.save()

    const found = await Asset.findOne({ where: { key: assetOld.key }, order: { version: 'DESC' } })

    expect(assetNew).toEqual(found)
  })
})

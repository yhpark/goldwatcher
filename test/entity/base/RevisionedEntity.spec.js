const { Entity, PrimaryGeneratedColumn } = require('typeorm')
const { RevisionedEntity, RevisionedCreateDateColumn, RevisionedUpdateDateColumn, RevisionedVersionColumn } = require('entity/base/RevisionedEntity')

@Entity()
class TestEntity extends RevisionedEntity {
  @PrimaryGeneratedColumn()
  id

  @RevisionedCreateDateColumn()
  created

  @RevisionedUpdateDateColumn()
  updated

  @RevisionedVersionColumn()
  version
}

describe('RevisionedEntity', () => {
  test('default version', () => {
    const entity = new TestEntity()

    expect(entity.version).toBe(0)
  })

  test('update version', () => {
    jest.spyOn(TestEntity, 'getRepository').mockReturnValue({
      hasId () { return true },
      metadata: { primaryColumns: [] }
    })

    const entity = new TestEntity()
    entity.version = 1
    expect(entity.version).toBe(1)

    const newEntity = entity.updateVersion()
    expect(newEntity.version).toBe(2)
  })
})

const config = require('config')
const path = require('path')

const NODE_PATH = process.env.NODE_PATH

module.exports = {
  type: 'sqlite',
  database: config.db,
  synchronize: true,
  entities: [path.join(NODE_PATH, 'entity/*.js')],
  migrations: ['migration/*.js'],
  cli: {
    entitiesDir: path.join(NODE_PATH, 'entity/'),
    migrationDir: 'migrations/'
  },
  logging: config.dbLogging
}

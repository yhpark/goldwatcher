const { BeforeInsert, BaseEntity, Column, UpdateDateColumn } = require('typeorm')

class RevisionedEntity extends BaseEntity {
  @BeforeInsert()
  fillCreated () {
    if (this[getColumnMetadata(this, KEY_VERSION_COLUMN).name] === 0) {
      this[getColumnMetadata(this, KEY_CREATE_DATE_COLUMN).name] = new Date()
    }
  }

  updateVersion () {
    if (!this.hasId()) {
      throw new Error('Should have an existing id to get updated version.')
    }

    const newVersion = Object.assign(Object.create(this), this)

    // set primary key to undefined
    this.constructor
      .getRepository()
      .metadata
      .primaryColumns
      .forEach(primaryColumn => primaryColumn.setEntityValue(newVersion, undefined))

    // set version++
    newVersion[getColumnMetadata(this, KEY_VERSION_COLUMN).name] = this.version + 1

    return newVersion
  }
}

const columnMetadata = []

function getColumnMetadata (object, key) {
  return columnMetadata
    .filter(metadata => metadata.target === object.constructor && metadata.key === key)
    .pop()
}

function setColumnMetadata (object, key, vals) {
  columnMetadata.push({
    target: object.constructor,
    key,
    ...vals
  })
}

const KEY_CREATE_DATE_COLUMN = 'CreateDate'
const KEY_UPDATE_DATE_COLUMN = 'UpdateDate'
const KEY_VERSION_COLUMN = 'Version'

function RevisionedCreateDateColumn () {
  return (object, propertyName, descriptor) => {
    if (getColumnMetadata(object, KEY_CREATE_DATE_COLUMN) !== undefined) {
      throw new Error('RevisonedCreateDateColumn is already defined.')
    }
    setColumnMetadata(object, KEY_CREATE_DATE_COLUMN, { name: propertyName })

    return Column('datetime').call(this, object, propertyName, descriptor)
  }
}

function RevisionedUpdateDateColumn () {
  return (object, name, descriptor) => {
    if (getColumnMetadata(object, KEY_UPDATE_DATE_COLUMN) !== undefined) {
      throw new Error('RevisonedUpdateDateColumn is already defined.')
    }
    setColumnMetadata(object, KEY_UPDATE_DATE_COLUMN, { name })

    return UpdateDateColumn().call(this, object, name, descriptor)
  }
}

function RevisionedVersionColumn () {
  return (object, name, descriptor) => {
    if (getColumnMetadata(object, KEY_VERSION_COLUMN) !== undefined) {
      throw new Error('RevisonedVersionColumn is already defined.')
    }
    setColumnMetadata(object, KEY_VERSION_COLUMN, { name })

    // set default version to 0
    if (!descriptor.initializer) {
      Object.assign(descriptor, {
        initializer: () => 0
      })
    }

    return Column('integer').call(this, object, name, descriptor)
  }
}

module.exports = {
  RevisionedCreateDateColumn,
  RevisionedUpdateDateColumn,
  RevisionedVersionColumn,
  RevisionedEntity
}

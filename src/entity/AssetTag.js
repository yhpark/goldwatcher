const { BaseEntity, Entity, PrimaryGeneratedColumn, Column, Index, ManyToOne, CreateDateColumn } = require('typeorm')

@Entity()
@Index('AssetTag-asset-tag-idx', ['asset', 'tag'], { unique: true })
class AssetTag extends BaseEntity {
  @PrimaryGeneratedColumn()
  id

  @ManyToOne(type => Asset, asset => asset.tags)
  asset

  @Column('varchar')
  tag

  @CreateDateColumn()
  created
}

module.exports = AssetTag

const Asset = require('./Asset')

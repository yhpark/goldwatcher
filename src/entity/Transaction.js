const { Entity, PrimaryGeneratedColumn, Column, ManyToOne } = require('typeorm')
const { RevisionedEntity, RevisionedCreateDateColumn, RevisionedUpdateDateColumn, RevisionedVersionColumn } = require('./base/RevisionedEntity')

const Asset = require('./Asset')

@Entity()
class Transaction extends RevisionedEntity {
  @PrimaryGeneratedColumn()
  id

  @ManyToOne(type => Asset, { nullable: true })
  from

  @ManyToOne(type => Asset, { nullable: true })
  to

  @Column('numeric')
  amount

  @Column('datetime')
  when

  @RevisionedCreateDateColumn()
  created

  @RevisionedUpdateDateColumn()
  updated

  @RevisionedVersionColumn()
  version
}

module.exports = Transaction

const { Entity, PrimaryGeneratedColumn, Column, Index, OneToMany } = require('typeorm')
const { RevisionedEntity, RevisionedCreateDateColumn, RevisionedUpdateDateColumn, RevisionedVersionColumn } = require('./base/RevisionedEntity')

@Entity()
@Index('Asset-key-version-idx', ['key', 'version'], { unique: true }) // change index to "key version DESC"
class Asset extends RevisionedEntity {
  @PrimaryGeneratedColumn()
  id = undefined

  @Column('varchar')
  key

  @Column('varchar')
  unit

  @OneToMany(type => AssetTag, assetTag => assetTag.asset)
  tags

  @RevisionedCreateDateColumn()
  created

  @RevisionedUpdateDateColumn()
  updated

  @RevisionedVersionColumn()
  version
}

module.exports = Asset

const AssetTag = require('./AssetTag')

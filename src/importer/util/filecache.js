const fs = require('fs')
const util = require('util')

const PATH_DIR = './tmp/'

function silent (func) {
  function fn (...args) {
    try {
      const returnVal = func.call(this, ...args)

      if (returnVal instanceof Promise) { // async
        return returnVal.catch(() => {}) // ignore
      } else { // sync
        return returnVal
      }
    } catch (e) {
      // ignore
    }
  }

  return fn
}

function cached (func, id) {
  const path = PATH_DIR + id + '.tmp'

  async function fn (...args) {
    const cacheRaw = await silent(util.promisify(fs.readFile))(path)
    const cache = await silent(JSON.parse)(cacheRaw)

    if (cache) {
      console.log('using cache')
      return cache
    }

    const returnVal = await func.call(this, ...args)

    await silent(util.promisify(fs.mkdir))(PATH_DIR)
    await silent(util.promisify(fs.writeFile))(path, JSON.stringify(returnVal))

    return returnVal
  }

  return fn
}

module.exports.cached = cached

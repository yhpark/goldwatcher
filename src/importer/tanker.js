const puppeteer = require('puppeteer')
const { cached } = require('./util/filecache')
const promptly = require('promptly')

const LOGIN_URL = 'https://tanker.fund/#/user_login'
const LANDING_URL = 'https://api.tanker.fund/main/landing/'
const TX_LIST_URL = `https://api.tanker.fund/vaccount/transaction/list/?filter_is_success=true&filter_is_pending=false`

const EMAIL_SELECTOR = '.content-wrapper input[type=\'email\']'
const PASSWORD_SELECTOR = '.content-wrapper input[type=\'password\']'
const SUBMIT_SELECTOR = '.content-wrapper button[type=\'submit\']'

const AUTH_TIMEOUT = 10000
const FETCH_TX_PAGE_COUNT = 100

function txListUrl (offset, count) {
  return TX_LIST_URL + `&offset=${offset}&count=${count}`
}

async function fetch () {
  const browser = await puppeteer.launch({ headless: true })
  const page = await browser.newPage()

  await page.goto(LOGIN_URL)

  await page.type(EMAIL_SELECTOR, await promptly.prompt('Email: '))
  await page.type(PASSWORD_SELECTOR, await promptly.prompt('Password: ', { silent: true }))

  await page.click(SUBMIT_SELECTOR)

  const authToken = await new Promise((resolve, reject) => {
    page.on('request', req => {
      if (req.url() === LANDING_URL && 'authorization' in req.headers()) {
        resolve(req.headers()['authorization'])
      }
    })
    // reject if login takes too long
    setTimeout(() => reject(new Error('timeout')), AUTH_TIMEOUT)
  })

  await page.setExtraHTTPHeaders({
    'Authorization': authToken,
    'Accept': 'application/json, text/plain, */*'
  })

  await page.goto(txListUrl(0, FETCH_TX_PAGE_COUNT))
  let content = JSON.parse(await page.evaluate(() => document.body.innerText))

  const txCount = content.transactions_count
  const transactions = Array(...content.transactions)

  console.log(transactions.length + '/' + txCount)

  while (transactions.length < txCount) {
    await page.goto(txListUrl(transactions.length, FETCH_TX_PAGE_COUNT))
    content = JSON.parse(await page.evaluate(() => document.body.innerText))
    transactions.push(...content.transactions)

    console.log(transactions.length + '/' + txCount)
  }

  await browser.close()

  return transactions
}

async function restructure (transactions) {
  // action
  // - 0 or 2: deposit/withdrawal
  // - 1 or 3: investment/interest

}

async function tanker () {
  const rawTransactions = await cached(fetch, 'tanker_tx')()
  const transactions = await restructure(rawTransactions)
  return transactions
}

module.exports = tanker

tanker().catch(console.error)
